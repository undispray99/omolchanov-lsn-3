# python-sample-app
Sample Python application on Flask with PostgreSQL database


##  System requirements
```shell
Vagrant 2.2.19+
Ansible 
```


##  Start project

1) Vagrant up
```shell
 > vagrant box add bionic-server-cloudimg-amd64-vagrant.box --name 'ubuntu/bionic64'
 > vagrant up
```
2) Run ansible-playbook
```shell
>  ansible-playbook -v -i inventory.yml start.yml 
```
3) Export VM and Run project
```shell
> vagrant ssh
> export FLASK_APP=/home/vagrant/src/app.py
> export POSTGRESQL_URL=postgresql://worker:worker@localhost/worker
> python3 src/app.py
```
4) Check project
```shell
> curl -i -X GET http://192.168.56.10:5000/api/user
```
5) Add DB
```shell
> curl -i -X POST -d '{ "username": "userrrr", "email": "user123@example.com", "password_hash": "example" }' -H "Content-Type: application/json" http://192.168.56.10:5000/api/user
```

API information
---

**Show users: `GET /api/user`**

* **URL**

  `/api/user`

* **Method**

  `GET`

* **URL Params**

  None

* **Data Params**

  None

* **Success Response**

  * **Code:** 200 <br />
    **Content:**

    ```[{"id": 1,"username": "user123", "email": "user@example.com","password_hash": "example"}]```

* **Error Response**

  None

* **Sample Call**

  `curl -i -X GET http://127.0.0.1:5000/api/user`

**Create user: `POST /api/user`**

* **URL**

  `/api/user`

* **Method**

  `POST`

*  **URL Params**

   None

* **Data Params**

  JSON with required fields: `username`, `email`, `password_hash`

* **Success Response**

  * **Code:** 200 <br />
    **Content:** `{ "id" : 12, "username" : "user123",  "email": "user@example.com", "password_hash": "example"}`

* **Error Response**

  * **Code:** 400 Bad Request <br />
    **Content:** `validate error`

* **Sample Call**

  `curl -i -X POST -d '{ "username": "user123", "email": "user@example.com", "password_hash": "example" }' -H "Content-Type: application/json" http://127.0.0.1:5000/api/user`

